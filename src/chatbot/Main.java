package chatbot;


import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.applet.AudioClip;
import java.awt.Color;
import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.time.LocalDateTime;

public class Main extends javax.swing.JFrame {

    String pregunta, respuesta, preguntagenerada;
    boolean reproducciendo = false;
    AudioClip sonido1, sonido2, alive;
    private Table1BR BRDeTable1 = new Table1BR();
    private Table2BR BRDeTable2 = new Table2BR();
    private Table3BR BRDeTable3 = new Table3BR();
    private Table4BR BRDeTable4 = new Table4BR();
    private Connection conn;
    private DataBaseConnection dataBaseConn = new DataBaseConnection();
    private Integer tablaActiva;
    
    private String extraños;
    private String fecha_hora_registro;
         private String medio_de_contacto;
   
    public Main() {
        initComponents();
        tablaActiva = 1; 
        try {
             conn = DataBaseConnection.conectar();
            //List<Table1Entity> tabla1 = BRDeTable1.getTabla1(conn);
            pantalla.append("Selecciona una opción");
            pantalla.append(System.lineSeparator());
            pantalla.append("Si decea salir solo escriba Exit");
            pantalla.append(System.lineSeparator());
           
            for (Table1Entity det : BRDeTable1.getTable1(conn)) {
                pantalla.append(det.getId().toString().concat("-").concat(det.getPreguntaTema()));
                pantalla.append(System.lineSeparator());
            }
        } catch (Exception e) {
            System.out.println(e.toString());
        } finally {
            try { 
                dataBaseConn.desconectar(conn);
            } catch (Exception ee) {
            }
     
        }
     
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        enviar = new javax.swing.JButton();
        texto = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        pantalla = new javax.swing.JTextArea();
        online = new javax.swing.JLabel();
        Namebot = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        enviar.setBackground(new java.awt.Color(255, 255, 255));
        enviar.setText("enviar");
        enviar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        enviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enviarActionPerformed(evt);
            }
        });

        texto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                textoActionPerformed(evt);
            }
        });

        pantalla.setEditable(false);
        pantalla.setColumns(20);
        pantalla.setFont(new java.awt.Font("Lucida Sans Unicode", 0, 12)); // NOI18N
        pantalla.setRows(5);
        jScrollPane1.setViewportView(pantalla);

        online.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N

        Namebot.setFont(new java.awt.Font("Tahoma", 0, 17)); // NOI18N
        Namebot.setText("Aketzali");

        jButton1.setText("Enseñar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(texto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(enviar))
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(Namebot)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(online, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 835, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(online, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(Namebot))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 259, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(enviar)
                    .addComponent(texto, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void enviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enviarActionPerformed
        // TODO add your handling code here:
        Thread hilo;
        hilo = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    conn = DataBaseConnection.conectar();
                    pregunta = texto.getText();
                    if (!pregunta.equals("4"))
                                 try{
                                     PreparedStatement Table_4_extraños = conn.prepareStatement("INSERT INTO Table_4_extraños (extraños, fecha_hora_registro, medio_de_contacto VALUES) (?, ?, ?);");
                                     pantalla.append("Explique el problema que tiene");
                                     pantalla.append(System.lineSeparator()); 
                                     BRDeTable4.setString(1, extraños); 
                                     BRDeTable4.setString(2, fecha_hora_registro);
                                     pantalla.append("Proporcione un medio de contacto para darle una respuesta a su problema");
                                     pantalla.append(System.lineSeparator()); 
                                     BRDeTable4.setString(3, medio_de_contacto); 
                                 }
                            catch (Exception e) {
                    System.out.println(e.toString());
                            }
                    if (tablaActiva.equals(1)) {
                        
                        for (Table2Entity det : BRDeTable2.getTable2ByFKTabla1(conn, Integer.valueOf(pregunta))) {
                            pantalla.append(det.getId().toString().concat("-").concat(det.getRespuesta_accion()));
                            pantalla.append(System.lineSeparator()); 
                        }
                        tablaActiva = 2;
                    } else if (tablaActiva.equals(2)) {
                        if (!pregunta.equals("11")) {
                            for (Table3Entity det : BRDeTable3.getTable3ByFKTabla2(conn, Integer.valueOf(pregunta))) {
                                pantalla.append(det.getId().toString().concat("-").concat(det.getRespuesta_accion()));
                                pantalla.append(System.lineSeparator());                       
                            }
                            tablaActiva = 3;
                        } else {
                            for (Table1Entity det : BRDeTable1.getTable1(conn)) {
                                pantalla.append(det.getId().toString().concat("-").concat(det.getPreguntaTema()));
                                pantalla.append(System.lineSeparator());
                            }
                            tablaActiva = 1;
                        }
                    } else if (tablaActiva.equals(3)) {

                    }
 

                } catch (Exception e) {
                    System.out.println(e.toString());
                } finally {
                    try {
                        dataBaseConn.desconectar(conn);
                    } catch (Exception ee) {
                    }
                }
            }
        });
        hilo.start();

    }//GEN-LAST:event_enviarActionPerformed

    public String generarpregunta() {
        int numero;
        numero = (int) (Math.random() * 9) + 1;
        String preguntaAleatoria = Integer.toString(numero);
        String preguntacompletada = preguntaAleatoria + "p";
        return preguntacompletada;
    }

    public int mitadProbabilidad() {
        int numero;
        numero = (int) (Math.random() * 9) + 1;
        return numero;
    }

    public void animacionEscribir(String respuestaxd) throws InterruptedException, URISyntaxException, IOException {
        sonido1 = java.applet.Applet.newAudioClip(getClass().getResource("./mensajellegada.wav"));
        sonido2 = java.applet.Applet.newAudioClip(getClass().getResource("./pop.wav"));
        Thread.sleep(generarRandom());
        online.setForeground(Color.blue);
        sonido2.play();
        online.setText("Visto");
        Thread.sleep(generarRandom());
        online.setText("Escribiendo");
        Thread.sleep(generarRandom());
        online.setText("");
        sonido1.play();
        pantalla.append("Aketzali: " + respuestaxd + "\n");
        if (pregunta.equalsIgnoreCase("")) {
        }
    }

    public void animacionpregunta(String respuestaxd) throws InterruptedException {
        sonido1 = java.applet.Applet.newAudioClip(getClass().getResource("./mensajellegada.wav"));
        Thread.sleep(1000);
        online.setText("Escribiendo");
        Thread.sleep(1000);
        online.setText("");
        sonido1.play();
        pantalla.append("Aketzali: " + respuestaxd + "\n");
    }

    public int generarRandom() {
        int numero;
        numero = (int) (Math.random() * 4000) + 1000;
        return numero;

    }

    public void fijarTexto() {
        pantalla.append("Maquina: " + respuesta + "\n");
        texto.setText("");
    }
    private void textoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_textoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_textoActionPerformed

     String insertSql = "INSERT INTO SalesLT.Product (Name, ProductNumber, Color, StandardCost, ListPrice, SellStartDate) VALUES "
                + "('NewBike', 'BikeNew', 'Blue', 50, 120, '2016-01-01');";
 
        ResultSet resultSet = null;
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
//        String UsuarioPregunta = JOptionPane.showInputDialog("Ingrese pregunta");
//        String respuestUsuarioPregunta = JOptionPane.showInputDialog("Que responder a '" + UsuarioPregunta + "'");
//        Leer aprender = new Leer();
//        String nuevapalabra = aprender.preguntanueva(UsuarioPregunta, respuestUsuarioPregunta);
//        aprender.guardar(aprender.leertxt("datos.txt"), nuevapalabra);
    }//GEN-LAST:event_jButton1ActionPerformed

    public JButton enviarPresionado() {
        return enviar;
    }

    public void setPanatalla() {
        pantalla.append("Maquina: " + respuesta + "\n");
    }

    public JTextArea regresaPantalla() {
        return pantalla;
    }

    public JTextField regresaTexto() {
        return texto;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Namebot;
    private javax.swing.JButton enviar;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel online;
    private javax.swing.JTextArea pantalla;
    private javax.swing.JTextField texto;
    // End of variables declaration//GEN-END:variables
}
