  /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatbot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author olver
 */
public class Table4BR {
    
     public List<Table4Entity> getTable4(Connection consql) throws SQLException {
       List<Table4Entity> lista = new ArrayList<>();
        PreparedStatement pssq4 = consql.prepareStatement("Tabla_4_respuestas");
        ResultSet rssq4 = pssq4.executeQuery();
        while (rssq4.next()) {
            Table4Entity xx = new Table4Entity();
            xx.setId(rssq4.getInt("id"));
            xx.setExtraños(rssq4.getString("extraños"));
            xx.setFecha_hora_registro(rssq4.getString("fecha_hora_registro"));
            xx.setMedio_de_contacto(rssq4.getString("medio_de_contacto"));
             
            
            
            lista.add(xx);
        }
        return lista;
    
    }

    void setString(int i, String extraños) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
