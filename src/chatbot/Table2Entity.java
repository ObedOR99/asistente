/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatbot;

import java.io.Serializable;

/**
 *
 * @author olver
 */
public class Table2Entity implements Serializable  {
        
         private Integer id;
         private String fk_tabla1_id;
         private String respuesta_accion;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the fk_tabla1_id
     */
    public String getFk_tabla1_id() {
        return fk_tabla1_id;
    }

    /**
     * @param fk_tabla1_id the fk_tabla1_id to set
     */
    public void setFk_tabla1_id(String fk_tabla1_id) {
        this.fk_tabla1_id = fk_tabla1_id;
    }

    /**
     * @return the respuesta_accion
     */
    public String getRespuesta_accion() {
        return respuesta_accion;
    }

    /**
     * @param respuesta_accion the respuesta_accion to set
     */
    public void setRespuesta_accion(String respuesta_accion) {
        this.respuesta_accion = respuesta_accion;
    }
    }
    

