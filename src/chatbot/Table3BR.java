/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatbot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author olver
 */
public class Table3BR {
    
     public List<Table3Entity> getTable3(Connection consql ) throws SQLException {
       List<Table3Entity> lista = new ArrayList<>();
        PreparedStatement pssql = consql.prepareStatement("select * from Table_3_respuestas");
        ResultSet rssq3 = pssql.executeQuery();
        while (rssq3.next()) {
            Table3Entity xx = new Table3Entity();
            xx.setId(rssq3.getInt("id"));
            xx.setFk_tabla2_id(rssq3.getString("fk__tabla2_id"));
            xx.setRespuesta_accion(rssq3.getString("respuesta_accion").trim());
            lista.add(xx);
        }
        return lista;
    
}
          public List<Table3Entity> getTable3ByFKTabla2(Connection consql,Integer FKtabal2) throws SQLException {
       List<Table3Entity> lista = new ArrayList<>();
        PreparedStatement pssql = consql.prepareStatement("select * from Table_3_respuestas where fk__tabla2_id=?");
        pssql.setInt(1, FKtabal2);
        ResultSet rssq3 = pssql.executeQuery();
        while (rssq3.next()) {
            Table3Entity xx = new Table3Entity();
            xx.setId(rssq3.getInt("id"));
            xx.setFk_tabla2_id(rssq3.getString("fk__tabla2_id"));
            xx.setRespuesta_accion(rssq3.getString("respuesta_accion").trim());
            lista.add(xx);
        }
        return lista;
    
}
}
