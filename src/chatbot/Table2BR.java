/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatbot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author olver
 */
public class Table2BR {
    public List<Table2Entity> getTable2(Connection consql) throws SQLException {
       List<Table2Entity> lista = new ArrayList<>();
        PreparedStatement pssq2 = consql.prepareStatement("select * from Table_2_preguntas_2");
        ResultSet rssq2 = pssq2.executeQuery();
        while (rssq2.next()) {
            Table2Entity xx = new Table2Entity();
            xx.setId(rssq2.getInt("id"));
            xx.setFk_tabla1_id(rssq2.getString("fk_tabla1_id"));
            xx.setRespuesta_accion(rssq2.getString("respuesta_accion1").trim());
             
            
            lista.add(xx);
        }
        return lista;
    
}
        public List<Table2Entity> getTable2ByFKTabla1(Connection consql,Integer FKTable1) throws SQLException {
       List<Table2Entity> lista = new ArrayList<>();
        PreparedStatement pssq2 = consql.prepareStatement("select * from Table_2_preguntas_2 where fk_tabla1_id is null or fk_tabla1_id=?");
        pssq2.setInt(1, FKTable1);
        ResultSet rssq2 = pssq2.executeQuery();
        while (rssq2.next()) {
            Table2Entity xx = new Table2Entity();
            xx.setId(rssq2.getInt("id"));
            xx.setFk_tabla1_id(rssq2.getString("fk_tabla1_id"));
            xx.setRespuesta_accion(rssq2.getString("respuesta_accion1").trim());
             
            
            lista.add(xx);
        }
        return lista;
    
}
}