/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatbot;

import java.io.Serializable; 

/**
 *
 * @author adrian
 */
public class Table1Entity implements Serializable  {
    
    private Integer id;
    private String preguntaTema;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the preguntaTema
     */
    public String getPreguntaTema() {
        return preguntaTema;
    }

    /**
     * @param preguntaTema the preguntaTema to set
     */
    public void setPreguntaTema(String preguntaTema) {
        this.preguntaTema = preguntaTema;
    }

       }
