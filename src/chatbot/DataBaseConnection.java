package chatbot;
// Importamos las clases de conexion a mysql y los drivers de sql

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DataBaseConnection { 

    // Declaramos la conexion a mysql
    private Connection con;
    // Declaramos los datos de conexion a la bd
    private final static String userDb = "sa";
    private final static String passDb = "Qazxsw96368525.*";
    private final static String classDb = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    private final static String urlDb = "jdbc:sqlserver://192.168.1.66:1433;database=Chatbot";
//    // Funcion que va conectarse a mi bd de mysql
//    public static void conector() {
//        // Reseteamos a null la conexion a la bd
//        con=null;
//        try{
//            Class.forName(driver);
//            // Nos conectamos a la bd
//            con= (Connection) DriverManager.getConnection(url, user, pass);
//            // Si la conexion fue exitosa mostramos un mensaje de conexion exitosa
//            if (con!=null){
//                
//                System.out.println("Conexion establecida()");
//            }
//        }
//        // Si la conexion NO fue exitosa mostramos un mensaje de error
//        catch (ClassNotFoundException | SQLException e){
//            
//            System.out.println("Error de conexion()"+ e.getMessage());
//        }
//    }
//     
//    
//    public static void main(String[] args) {		
//		
//                conector();
//	}

    public static Connection conectar() throws SQLException, ClassNotFoundException {
        Connection conexion;
        Class.forName(classDb);
        conexion = DriverManager.getConnection(urlDb, userDb, passDb);
        return conexion;
    }

    public void desconectar(Connection conexion) throws SQLException, ClassNotFoundException {
        if (conexion != null) {
            conexion.close();
        }
    }
}
