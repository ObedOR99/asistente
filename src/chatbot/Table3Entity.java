/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatbot;

import java.io.Serializable;

/**
 *
 * @author olver
 */
public class Table3Entity implements Serializable{
    
        
         private Integer id;
         private String fk_tabla2_id;
         private String respuesta_accion;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the fk_tabla2_id
     */
    public String getFk_tabla2_id() {
        return fk_tabla2_id;
    }

    /**
     * @param fk_tabla2_id the fk_tabla2_id to set
     */
    public void setFk_tabla2_id(String fk_tabla2_id) {
        this.fk_tabla2_id = fk_tabla2_id;
    }

    /**
     * @return the respuesta_accion
     */
    public String getRespuesta_accion() {
        return respuesta_accion;
    }

    /**
     * @param respuesta_accion the respuesta_accion to set
     */
    public void setRespuesta_accion(String respuesta_accion) {
        this.respuesta_accion = respuesta_accion;
    }
    

}
