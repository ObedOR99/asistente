/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatbot;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author adrian
 */
public class Table1BR {
    

    public List<Table1Entity> getTable1(Connection consql) throws SQLException {
       List<Table1Entity> lista = new ArrayList<>();
        PreparedStatement pssql = consql.prepareStatement("select * from Table_1_preguntas");
        ResultSet rssql = pssql.executeQuery();
        while (rssql.next()) {
            Table1Entity xx = new Table1Entity();
            xx.setId(rssql.getInt("id"));
            xx.setPreguntaTema(rssql.getString("pregunta_tema").trim());
            lista.add(xx);
        }
        return lista;
    }

    
    
    
}
