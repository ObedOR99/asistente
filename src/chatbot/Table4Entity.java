/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package chatbot;

import java.io.Serializable;

/**
 *
 * @author olver
 */
public class Table4Entity implements Serializable  {
         private Integer id;
         private String extraños;
         private String fecha_hora_registro;
         private String medio_de_contacto;

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the extraños
     */
    public String getExtraños() {
        return extraños;
    }

    /**
     * @param extraños the extraños to set
     */
    public void setExtraños(String extraños) {
        this.extraños = extraños;
    }

    /**
     * @return the fecha_hora_registro
     */
    public String getFecha_hora_registro() {
        return fecha_hora_registro;
    }

    /**
     * @param fecha_hora_registro the fecha_hora_registro to set
     */
    public void setFecha_hora_registro(String fecha_hora_registro) {
        this.fecha_hora_registro = fecha_hora_registro;
    }

    /**
     * @return the medio_de_contacto
     */
    public String getMedio_de_contacto() {
        return medio_de_contacto;
    }

    /**
     * @param medio_de_contacto the medio_de_contacto to set
     */
    public void setMedio_de_contacto(String medio_de_contacto) {
        this.medio_de_contacto = medio_de_contacto;
    }
 }

